# Gitlab Mux VSCode plugin

A VSCode plugin for https://gitlab.com/gitlab-org/secure/vulnerability-research/pocs/gitlab-mux. The plugin can be installed by running `build.sh`.
